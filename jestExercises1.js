// Welcome to testing with Jest, you scrubs. Now get to work!

/////////////////////////
//Exercise 1:
/////////////////////////

//Write a function that adds two numbers.

function addNumbers (num1, num2) {
  return num1 + num2;
}


/////////////////////////
//Exercise 2:
/////////////////////////

//Write a function that returns a null value.

//function getNull

/////////////////////////
//Exercise 3:
/////////////////////////

//Write a function that returns an object with the keys 'firstName' and 'lastName'. Use
//your own name to fill in the values for each key.

//function fetchUser


/////////////////////////
//Exercise 4:
/////////////////////////

//Write a function that returns the array ['bumblebee', 'porpoise', 'pennywise', 'porkchops'].

//function makeFriends


/////////////////////////
//Exercise 5:
/////////////////////////

/*
From this point onwards, follow the Test Driven Development (TDD) methodology:
Step 1: Write tests describing expected behavior and checking for fail cases.
Step 2: Launch tests and ensure that they all fail.
Step 3: Write the function.
Step 4: Launch tests and ensure that they all pass.
*/

//Write a function that reverses a string.
//HINT: Try using the string methods .split(''), .reverse(), and .join('').

// function revString


/////////////////////////
//Exercise 6:
/////////////////////////

// Write a function that calls the API: https://yesno.wtf/api/

// Import a module to perform HTTP requests and parsing for us.
const axios = require('axios');

/*
'axios.get()' makes an HTTP GET request to the provided URL and returns a promise for an object.
'.then()' takes two arguments, a callback for a success case and another for the failure case.
Both are optional.
*/


function YesNo () { axios.get('https://yesno.wtf/api/')
  .then(
    (data) => { console.log(data); }
  )
  .catch(
    (err) => { console.log('Fetch Error: ', err); }
  )};

/*
Step 1: Write tests for YesNo in the test file.
Step 2: Launch tests and ensure that they all fail.
Step 3: Modify YesNo so that it actually returns the object to its caller.
Step 4: Launch tests and ensure that they all pass.
*/


/////////////////////////
/////////////////////////

// Export bindings.
module.exports = { addNumbers, /* getNull, fetchUser, makeFriends, revString,*/ YesNo };
