// Welcome to testing with Jest, you scrubs. Now get to work!

// Import bindings.
exercises = require('./jestExercises1.js');


/////////////////////////
//Exercise 1:
/////////////////////////

//Write tests for a function that adds two numbers.

it('should compute 2 + 2 and result in 4', function () {
  expect(exercises.addNumbers(2, 2)).toBe(4);
})

test('if 2 + 2 does NOT result in 5', function () {
  expect(exercises.addNumbers(2, 2)).not.toBe(5);
})

/////////////////////////
//Exercise 2:
/////////////////////////

/*
Write tests for a function that returns a null value.
Step 1: Use the matcher '.toBeFalsy()' to check if the returned value is falsy.
It takes no arguments.
Step 2: Use the matcher '.toBeNull()' to check if the function returns a null value.
It takes no arguments.
*/


/////////////////////////
//Exercise 3:
/////////////////////////

/*
Write a test for a function that returns an object with the keys 'firstName'
and 'lastName' paired to your own name.
Step 1: Try using the matcher '.toBe()'. Does it work? Why or why not?
Step 2: Try using the matcher '.toEqual()'. Does it work? Why or why not?
*/


/////////////////////////
//Exercise 4:
/////////////////////////

/*
Write a test for a function that returns an array. Check to see if it contains
the string 'porkchops'.
HINT: Try using the matcher '.toContain()'.
*/


/////////////////////////
//Exercise 5:
/////////////////////////

/*
From this point onwards, follow the Test Driven Development (TDD) methodology:
Step 1: Write tests describing expected behavior and checking for fail cases.
Step 2: Launch tests and ensure that they all fail.
Step 3: Write the function.
Step 4: Launch tests and ensure that they all pass.
*/

/*
Write tests for a function that reverses a string.
Step 1: Check if the output of the function is defined (i.e. not undefined).
HINT: Use the matcher '.toBeDefined()'.
Step 2: Check if an example string is properly reversed.
*/


/////////////////////////
//Exercise 6:
/////////////////////////

/*
Write a test for a function that calls the API: https://yesno.wtf/api/
Check to make sure the object returned has a value of 'yes' or 'no' for the key
'answer'.
*/

/*
test('if answer is either yes or no', () => {
  return expect(exercises.YesNo['answer'] == someCondition ).toBeTruthy();
});
*/
