// Welcome to testing with Jest, you scrubs. Now get to work!

// Import bindings.
exercises = require('./jestExercises2.js');


/////////////////////////
//Exercise 1:
/////////////////////////

/*
Follow the Test Driven Development (TDD) methodology:
Step 1: Write tests describing expected behavior and checking for fail cases.
Step 2: Launch tests and ensure that they all fail.
Step 3: Write the function.
Step 4: Launch tests and ensure that they all pass.
*/

/*
Write tests for a reducer that outputs the sum of an array of numbers.
NOTE: It's okay if it also works on strings.
*/


/////////////////////////
//Exercise 2:
/////////////////////////

//Write tests for a reducer that flattens arbitrarily nested arrays of numbers.

const numArray = [1, 2, [3, 10, [11, 12]], [1, 2, [3, 4]], 5, 6];


/////////////////////////
//Exercise 3:
/////////////////////////

/*
Write tests for an async call to a database of Pokemon.

Step 1: Make use of before/after and describe wrappers to do some setup. You need
to "run" your database service before you can test your function, i.e. change
the 'status' variable to 1.
*/

/*Do not remove the environment variable below. It is to simulate the initial
state of your "database service".
*/
status = 0;

/*
HINT:
describe('this block of tests', () => {
  beforeAll( () => {
    launchYourDatabaseService(); });
  test('something', () => {
    ···
  });
  test('something else', () => {
    ···
  });
  afterAll( () => {
    killYourDatabaseService(); })
})
*/

/*
Step 2: Write your damn tests already.
*/


/////////////////////////
//Exercise 4:
/////////////////////////

/*
Write tests for a function that takes a Pokemon object and turns it into a nested
object.
*/


/////////////////////////
//Exercise 5:
/////////////////////////

/*
Write an integration test that checks to see if the Pokemon database is working
as expected and that rekeyObject() is outputting modified Pokemon objects as
expected.
*/
