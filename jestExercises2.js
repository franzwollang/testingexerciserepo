// Welcome to testing with Jest, you scrubs. Now get to work!

/////////////////////////
//Exercise 1:
/////////////////////////

/*
Follow the Test Driven Development (TDD) methodology:
Step 1: Write tests describing expected behavior and checking for fail cases.
Step 2: Launch tests and ensure that they all fail.
Step 3: Write the function.
Step 4: Launch tests and ensure that they all pass.
*/

//Write a reducer that sums an array of numbers.


/////////////////////////
//Exercise 2:
/////////////////////////

/*
Write a function that will take an arbitrarily nested array of numbers and output
a flat array of numbers.
Step 1: Try using the .flat() array method; remember to write your tests first.
Step 2: The .flat() array method is banned. Reimplement it yourself.
*/

/*
HINT for Step 2: Write a reducer. Instead of making the accumulator a single
number, make it a single array that will have numbers added to it by the reducer.
The reducer should check if the value it's looking at is a nested array; if it is,
it should recursively call itself, i.e. pass that nested array to a call to
itself. If the value is not a nested array, it should concatenate the number to
the accumulator.
*/

//function flattenArray(data) {}


/////////////////////////
//Exercise 3:
/////////////////////////

/*
Step 1: Write a function that returns the following pokemon object:
  const pokemon = [
    { name: "charmander", type: "fire" },
    { name: "squirtle", type: "water" },
    { name: "bulbasaur", type: "grass" }
  ];
*/

//function databaseAccess() {}

/*
Step 2: The previous function is mimicking a database access for us. Write new
function that makes this simulation for realistic by returning a promise.
Include a condition that checks for a binary "status" variable; this simulates
the database service being turned on or off.
In other words, "make an async call to your database service to retrieve a
specific entry".
*/

/*
function databaseCall () {
  let promise = new Promise(function(resolve, reject) {
    result = databaseAccess();
    if ( status ) {
      resolve(result);
    }
    else {
      reject(Error("404 - Not Found."));
    }
    });
};
*/


/////////////////////////
//Exercise 4:
/////////////////////////

/*
Write a function that takes the original Pokemon object and outputs the modified
version below:
  const pokemonModified = {
    charmander: { type: "fire" },
    squirtle: { type: "water" },
    bulbasaur: { type: "grass" }
  };

Step 1: Write a reducer that does this.
Step 2: Wrap it in a funcion that returns the result of calling the reduce
method using the new reducer.
*/

/*
const rekeyObject = (data) =>
  return data.reduce((acc, item) => {
    //add object key to our object i.e. charmander: { type: 'water' }
    acc[item.name] = { type: item.type };
  }, {});
*/


/////////////////////////
/////////////////////////

// Export bindings.
module.exports = {};
